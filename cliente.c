/*
 *			C L I E N T C P
 *
 *	This is an example program that demonstrates the use of stream sockets as an IPC mechanism.  
 *  This contains the client, and is intended to operate in conjunction with the server program.  
 *  Together, these two programs demonstrate many of the features of sockets, as well as good
 *	conventions for using these features.
 */
 
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <time.h>

#define PUERTO 6179
#define TAM_BUFFER 30

int main(argc, argv)int argc;char *argv[];
{
	int s;	/* connected socket descriptor */
	/* struct hostent *hp;pointer to host info for remote host */
	struct addrinfo hints, *res;
	long timevar;/* contains time returned by time() */
	struct sockaddr_in myaddr_in;	/* for local socket address */
	struct sockaddr_in servaddr_in;	/* for server socket address */
	int addrlen, i, j, errcode;
	char buf[TAM_BUFFER];/* This example uses TAM_BUFFER byte messages. */
  	FILE * fp;
	char buff_lectura[TAM_BUFFER];
 int contador_lectura = 0;
	if (argc != 4) {
		printf("Uso: %s <remote host> <TCP/UDP> <ordenes.txt/ordenes1.txt/ordenes2.txt>\n", argv[0]);
		//fprintf(stderr, "Usage:  %s <remote host>\n", argv[0]);
		exit(1);
	}
	if( strcmp(argv[2], "TCP") && strcmp(argv[2], "UDP") ) {
		printf("Uso: %s <remote host> <TCP/UDP> <ordenes.txt/ordenes1.txt/ordenes2.txt>\n", argv[0]);
		return 2;
	}

	if( strcmp(argv[3], "ordenes.txt") && strcmp(argv[3], "ordenes1.txt") && strcmp(argv[3], "ordenes2.txt") ) {
		printf("Uso: %s <remote host> <TCP/UDP> <ordenes.txt/ordenes1.txt/ordenes2.txt>\n", argv[0]);
		return 3;
	} 
	fp= fopen(argv[3], "r+");
	if(fp == NULL)
	{
		perror("fopen");
		fprintf(stderr, "\nError al abrir el fichero.\n");
		return -1;    
	}
	

  if ( !strcmp(argv[2], "TCP") ){
		/* clear out address structures */
		memset ((char *)&myaddr_in, 0, sizeof(struct sockaddr_in));
		memset ((char *)&servaddr_in, 0, sizeof(struct sockaddr_in));
		
		servaddr_in.sin_family = AF_INET;
		memset (&hints, 0, sizeof (hints));
		hints.ai_family = AF_INET;
		/* esta funciï¿½n es la recomendada para la compatibilidad con IPv6 gethostbyname queda obsoleta*/
		errcode = getaddrinfo (argv[1], NULL, &hints, &res); 
		if (errcode != 0)
		{  	/* Name was not found.  Return a special value signifying the error.*/
			fprintf(stderr, "%s: No es posible resolver la IP de %s\n",
			argv[0], argv[1]);
			exit(1);
		}
		else {
			/* Copy address of host */
			servaddr_in.sin_addr = ((struct sockaddr_in *) res->ai_addr)->sin_addr;
		}
		freeaddrinfo(res);
		servaddr_in.sin_port = htons(PUERTO);

		/* Create the socket. */
		s = socket (AF_INET, SOCK_STREAM, 0);
		if (s == -1) {
			perror(argv[0]);
			fprintf(stderr, "%s: unable to create socket\n", argv[0]);
			exit(1);
		}

		if (connect(s, (const struct sockaddr *)&servaddr_in, sizeof(struct sockaddr_in)) == -1) {
			perror(argv[0]);
			fprintf(stderr, "%s: unable to connect to remote\n", argv[0]);
			exit(1);
		}

		addrlen = sizeof(struct sockaddr_in);
		if (getsockname(s, (struct sockaddr *)&myaddr_in, &addrlen) == -1) {
			perror(argv[0]);
			fprintf(stderr, "%s: unable to read socket address\n", argv[0]);
			exit(1);
		}

		/* Print out a startup message for the user. */
		time(&timevar);
		printf("Connected to %s on port %u at %s",argv[1], ntohs(myaddr_in.sin_port), (char *) ctime(&timevar));
		/*EJEMPLO CLASE*/
		/*
		for (i=1; i<=5; i++) {
			*buf = i;
			if (send(s, buf, TAM_BUFFER, 0) != TAM_BUFFER) {
				fprintf(stderr, "%s: Connection aborted on error ",argv[0]);
				fprintf(stderr, "on send number %d\n", i);
				exit(1);
			}
		}*/
		while (fgets(buf, TAM_BUFFER, fp) != NULL){
			/* Aquï¿½ tratamos la lï¿½nea leï¿½da */
			//printf("cliente> %s", buffer);		
			if (send(s, buf, TAM_BUFFER, 0) != TAM_BUFFER) {
				fprintf(stderr, "%s: Connection aborted on error ", argv[0]);
				fprintf(stderr, "on send number %d\n", i);
				return 7;
			}
		} 
	 	close(fp);
		if (shutdown(s, 1) == -1) {
			perror(argv[0]);
			fprintf(stderr, "%s: unable to shutdown socket\n", argv[0]);
			exit(1);
		}

		while (i = recv(s, buf, TAM_BUFFER, 0)) {
			if (i == -1) {
				perror(argv[0]);
				fprintf(stderr, "%s: error reading result\n", argv[0]);
				exit(1);
			}
			while (i < TAM_BUFFER) {
				j = recv(s, &buf[i], TAM_BUFFER-i, 0);
				if (j == -1) {
					perror(argv[0]);
					fprintf(stderr, "%s: error reading result\n", argv[0]);
					exit(1);
				}
				i += j;
			}
			/* Print out message indicating the identity of this reply.*/
			printf("Received result number %s \n", buf);
		}
  }
  else 
  {
    printf("MODO UDP. FALTA IMPLEMETAR\n");
  }
	/* Print message indicating completion of task. */
	time(&timevar);
	printf("All done at %s", (char *)ctime(&timevar));
}
