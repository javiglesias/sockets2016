/*
    estas son las pruebas de las operaciones que tiene que hacer el cliente antes
    de enviar la informacion al servidor.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int main(int args, char **argv[])
{
    char buffer_lectura[250] = "";
    char buffer_fichero[250] ="/www";
    bool modo=0; //1 con conexion, 0 sin conexion
    FILE *f = 0;
    if(args < 2)
    {
        printf("Use mode: %s <ordenes.txt> <tcp/udp> <server> ", argv[0]);
        exit(-2);
    }
    f = fopen(argv[1], "r");
    if(f == -1)
        printf("Error abriendo el archivo.");
    else
    {
        printf("\n-> %i", fscanf(f,"%s", buffer_lectura));
        printf("\n-> %s", buffer_lectura);
        if(strcmp(buffer_lectura,"GET") == 0)
            printf("\nHe recibido un GET.");
        printf("\n-> %i", fscanf(f,"%s", buffer_lectura));
        printf("\n-> %s", buffer_lectura);
        strcat(buffer_fichero, buffer_lectura);
        fscanf(f,"%s", buffer_lectura);
        if(strcmp(buffer_lectura, "k") == 0)
            modo = 1;
        else
            modo = 0;
        //fclose(f);
        /*f = fopen(buffer_fichero, "r");
        if(f == -1)
            printf("\nNombre de archivo incorrecto. %s", buffer_fichero);
        else
            printf("\nNombre de fichero correcto. %i", f);
        printf("\n-> %i", fscanf(f,"%s", buffer_fichero));
        printf("\n-> %s", buffer_fichero);*/
    }
    if(modo)
        printf("\nmodo conexion");
    else
        printf("\nmodo sin conexion");
    fclose(f);
    return 0;
}
